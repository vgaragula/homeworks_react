import { useEffect, useState } from "react";
import ProductList from "../components/product/productList";
import "./favorites-page.scss";
import { Link } from "react-router-dom";

const Favorites = ({ list, favoritesToggle, openModal, basket }) => {
  const [isListEmpty, setIsListEmpty] = useState(true);

  useEffect(() => {
    if (list.length === 0) {
      setIsListEmpty(true);
    } else {
      setIsListEmpty(false);
    }
  }, [list.length]);

  return (
    <>
      <div className="favorites-container">
        <h2 className="favorites-title">Обрані товари:</h2>
        {isListEmpty ? (
          <div className="favorites-empty-list">
            <h3 className="favorites-empty-list_title">
              Ваш лист бажань порожній
            </h3>
            <img
              className="favorites-empty-list_img"
              src="./img/empty-list.png"
              alt="empty list"
            />
            <p className="favorites-empty-list_text">
              Ви можете поповнити його
            </p>
            <Link to="/">
              <button className="favorites-empty-list_btn">На головну</button>
            </Link>
          </div>
        ) : (
          <ProductList
            list={list}
            favoritesToggle={favoritesToggle}
            openModal={openModal}
            basket={basket}
            favorites={list}
          />
        )}
      </div>
    </>
  );
};

export default Favorites;
