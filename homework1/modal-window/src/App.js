import React, {Component} from 'react';

import Header from './components/Header';
import Modal from './components/Modals/Modal';


import './App.css';

class App extends Component {

    state = {
      first:false,
      second:false
    }
    // this.modal1=this.modal1.bind(this)
    // this.modal2=this.modal1.bind(this)

  modal1 = ()=> {
this.setState({first:true})

  }
  modal2 = ()=> {
    this.setState({second:true})
  }
  hideModalFirst= () => {

    this.setState({first:false})
}
hideModalSecond= () => {

  this.setState({second:false})
}
  render() {
    let firstModal = null
    let secondModal = null
    if(this.state.first) {
  firstModal = <Modal show={this.state.first} handleClose={this.hideModalFirst} text={"Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"} title= 'Do you want to delete this file?'
  classNameModalMain={"modal-main"}/>
}
if(this.state.second) {
  secondModal = <Modal show={this.state.second} handleClose={this.hideModalSecond} text={"The page will reload when you make changes.You may also see any lint errors in the console."} title={'In the project directory, you can run:'}
  classNameModalMain={"modal-main2"} />
}
    return ( 
    <div>
    <Header openModal1={this.modal1}  openModal2={this.modal2} />
   {firstModal}
   {secondModal}
    </div>
    )
  }
  }
  

export default App;
