import React, { Component } from 'react'
import Buttons from '../Buttons';

export default class Header extends Component {


  render() {

   const {openModal1, openModal2} = this.props
   const buttonOpenFirst = <Buttons backgroundColor="#282c34" text="Open first Modal " onClick = {openModal1}/>;
   const buttonOpenSecond = <Buttons backgroundColor="#282c34" text="Open second Modal" onClick = {openModal2}/>;
    return (
        <div className="header">
{buttonOpenFirst}
{buttonOpenSecond}
            
      </div> 


    )
  }
}
