import React, { Component } from 'react'
import './modal.css'
import Buttons from '../../Buttons'
import { ReactComponent as CloseLogo } from './cross.svg'

export default class Modal extends Component {

   
  render() {
const {show,handleClose,text,title, classNameModalMain } = this.props
    
    const buttonFirst = <Buttons backgroundColor="rgb(0 0 0 / 20%)" text="Oк" onClick = {handleClose}/>;
    const buttonSecond = <Buttons backgroundColor="rgb(0 0 0 / 20%)" text="Cancel" onClick = {handleClose}/>;
    const buttonLogo = <CloseLogo backgroundColor="rgb(0 0 0 / 20%)" text="Cancel" onClick = {handleClose}/>;
        return (
        <div className={show} onClick={handleClose} >
            <div className='modal'>
        <div className={classNameModalMain}>
            <header className='header-modal'><h2>{title}</h2>{buttonLogo}</header>
            <main className='modal-content'>
            <p>{text}</p>
            </main>
           
            <footer className='footer-modal'>
                {buttonFirst} {buttonSecond}
            </footer>
            </div>
        </div>
        </div>
    )
  }
}